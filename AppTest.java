package sa.edu.kau.fcit.cpit252;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class AppTest 
{

    @Test
    public void shouldHaveSingleThreadPool()
    {
        MyThreadPool myThreadPool1 = MyThreadPool.insThreadPool();
        MyThreadPool myThreadPool2 = MyThreadPool.insThreadPool();
        assertEquals(myThreadPool1, myThreadPool2);
    }
}
