package sa.edu.kau.fcit.cpit252;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyThreadPool {
    private ExecutorService executorService;
    private final int THREAD_POOL_SIZE = 5;
    private static MyThreadPool insThreadPool = null;

    public MyThreadPool(){
        this.executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
    }

    public ExecutorService getThreadPoolExecutor(){
      
        return this.executorService;
    }
    
    public static MyThreadPool insThreadPool(){
      
        if(insThreadPool == null){
        insThreadPool = new MyThreadPool();
        }
        return insThreadPool;
    }

}
